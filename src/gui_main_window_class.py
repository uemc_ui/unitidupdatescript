# 10/14/19 Quinlin Riggs

import os

from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QWidget, QApplication, QPushButton, QLabel, \
                            QVBoxLayout, QFileDialog, QCalendarWidget, \
                            QDateTimeEdit, QCheckBox, QSpinBox, QLineEdit, \
                            QFormLayout, QGridLayout

class Window(QWidget):
    """Main Window Widget"""

    # Custom signals
    signalDirectory      = QtCore.pyqtSignal(str)
    signalUnitID         = QtCore.pyqtSignal(int)
    signalSmartPartNum   = QtCore.pyqtSignal(str)

    def __init__(self):
        QWidget.__init__(self)

        self.setWindowTitle('Update Unit Information')

        self.directory    = None
        self.unitid       = None
        self.smartpartnum = None

        # Input Fields
        self.line_directory    = QLineEdit()
        self.line_unitid       = QLineEdit()
        self.line_smartpartnum = QLineEdit()

        # QLine Edit Input Validation
        self.line_directory.setReadOnly(True)
        self.line_directory.setAlignment(QtCore.Qt.AlignRight)

        self.line_unitid.setValidator(QtGui.QIntValidator(0, 99999999))

        self.line_unitid.setMaxLength(8)
        self.line_smartpartnum.setMaxLength(20)

        self.line_unitid.setAlignment(QtCore.Qt.AlignRight)
        self.line_smartpartnum.setAlignment(QtCore.Qt.AlignRight)

        # Button Widgets
        self.button_directory = QPushButton('Choose Unit Clone Directory', self)
        self.button_start     = QPushButton('Update Unit Information', self)
        #self.button_cancel    = QPushButton('Kill Thread (', self)
        self.label_status     = QLabel('Waiting For Input', self)

        # Line Edit Sub Layout
        flo = QFormLayout()
        flo.addRow('Directory', self.line_directory)
        flo.addRow('Unit ID', self.line_unitid)
        flo.addRow('Smart Part Number', self.line_smartpartnum)

        # Main Layout
        self.layout = QGridLayout(self)
        self.layout.addWidget(self.button_directory, 0, 1)
        self.layout.addLayout(flo, 1, 1)
        self.layout.addWidget(self.label_status, 2, 1)
        self.layout.addWidget(self.button_start, 3, 1)
        #self.layout.addWidget(self.button_cancel, 4, 1)

        self.setLayout(self.layout)
        self.setGeometry(150, 150, 800, 500)

    # Slotted Functions, recieve emit from worker, etc
    @QtCore.pyqtSlot(str)
    def updateStatus(self, status):
        self.label_status.setText(status)

    # Socket Functions 
    def updateDirectory(self, dir):
        dialog = QFileDialog()
        self.directory = dialog.getExistingDirectory(None, "Select Folder Containing XML file", os.getcwd())
        self.line_directory.setText('{0}'.format(self.directory))
        self.signalDirectory.emit(self.directory)

    def updateUnitID(self):
        self.unitid = int(self.line_unitid.text())

        # Custom Validations
        if self.unitid > 16777215:
            self.unitid = 16777215
            self.line_unitid.setText('16777215')
        if self.unitid < 0:
            self.unitid = 0
            self.line_unitid.setText('0')

        self.signalUnitID.emit(self.unitid)

    def updateSmartPartNum(self):
        # Custom Validations
        self.smartpartnum = self.line_smartpartnum.text().ljust(20)

        self.signalSmartPartNum.emit(self.smartpartnum)