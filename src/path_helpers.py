# 10/14/19 Quinlin Riggs

import os

from pathlib import Path

def get_sql_log_path(top_path: Path) -> Path:
    """Return a path to the associated sql db 
    
    Args:
        top_path      : top directory containing sql database

    Returns:
        return_list[0]: Path of sql database

    """

    return_list = []

    for root, dirs, files in os.walk(top_path, topdown=True):
        for file in files:
            if file.endswith(('sql.sqlite', '.sqlite')):
                return_list.append(Path(os.path.join(root, file)))
    if return_list:
        # return a single file
        return return_list[0]
    else:
        return None

def get_xml_path(top_path: Path) -> Path:
    """Return a path to the associated xml file 
    
    Args:
        top_path      : top directory containing xml database

    Returns:
        return_list[0]: Path of xml
    """

    return_list = []
    for root, dirs, files in os.walk(top_path, topdown=True):
        for file in files:
            if file.endswith(('ExportClone.xml')):
                # File must be literal
                return_list.append(Path(os.path.join(root, file)))

    if return_list:
        # return a single file
        return return_list[0]
    else:
        return None
