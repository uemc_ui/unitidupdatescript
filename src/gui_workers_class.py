# 10/14/19 Quinlin Riggs

from PyQt5 import QtCore

import core

class WorkerObject(QtCore.QObject):
    """Worker Object for new thread"""

    signalStatus = QtCore.pyqtSignal(str)

    def __init__(self, dir, unitid, smartpartnum , parent=None):
        super(self.__class__, self).__init__(parent)
        self.directory    = dir
        self.unitid       = unitid
        self.smartpartnum = smartpartnum 

    @QtCore.pyqtSlot()        
    def startWork(self):
        # Start external scipt and return data from analysis
        if self.directory and self.unitid and self.smartpartnum:
            self.signalStatus.emit('Running...')
            done = core.main(top_dir=self.directory, 
                             unitid=self.unitid, 
                             smartpartnum=self.smartpartnum)
            if done:
                self.signalStatus.emit('Unit Information Updated.')
            else:
                self.signalStatus.emit('Error: Unable to update XML')
        else:
            self.signalStatus.emit('Set Directory, UnitID and SmartPart Number')

    @QtCore.pyqtSlot(str)
    def updateDirectory(self, dir):
        self.directory = dir

    @QtCore.pyqtSlot(int)
    def updateUnitID(self, id):
        self.unitid = id

    @QtCore.pyqtSlot(str)
    def updateSmartPartNum(self, num):
        self.smartpartnum = num
