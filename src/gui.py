#!/usr/bin/env python3

# 10/14/19 Quinlin Riggs

import sys

from gui_app_class import App

import PyQt5

from PyQt5.QtWidgets import QApplication

if __name__=='__main__':
    app = QApplication(sys.argv)
    main_app = App(app)
    sys.exit(app.exec_())

