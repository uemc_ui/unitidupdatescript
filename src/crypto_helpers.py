# 10/14/19 Quinlin Riggs

# Port of OpenSSl Functions for AES 256 CBC 
# OpenSSL functions for encrypting and decrypting
# https://github.com/openssl/openssl/blob/6f0ac0e2f27d9240516edb9a23b7863e7ad02898/crypto/evp/evp_key.c#L74
import sys
import os
import io
import hashlib
import binascii
import argparse
import logging
import getpass
import base64
from Crypto.Cipher import (AES, ARC2, ARC4, Blowfish, CAST, DES, DES3)
#pycrypto or pycryptodome (windows fork)

OPENSSL_ENC_MAGIC = b'Salted__'
PKCS5_SALT_LEN = 8

def EVP_BytesToKey(key_length: int, iv_length: int, md, salt: bytes, data: bytes, count: int=1) -> (bytes, bytes):
    """
    See:
        https://github.com/openssl/openssl/blob/6f0ac0e2f27d9240516edb9a23b7863e7ad02898/crypto/evp/evp_key.c#L74
    """
    assert data
    assert salt == b'' or len(salt) == PKCS5_SALT_LEN

    md_buf = b''
    key = b''
    iv = b''

    addmd = 0

    while key_length > len(key) or iv_length > len(iv):
        c = md()
        if addmd:
            c.update(md_buf)
        addmd += 1
        c.update(data)
        c.update(salt)
        md_buf = c.digest()
        for i in range(1, count):
            md_buf = md(md_buf)

        md_buf2 = md_buf

        if key_length > len(key):
            key, md_buf2 = key + md_buf2[:key_length - len(key)], md_buf2[key_length - len(key):]

        if iv_length > len(iv):
            iv = iv + md_buf2[:iv_length - len(iv)]

    return key, iv

def openssl_enc_encrypt(input: io.BytesIO, output: io.BytesIO, passphrase: bytes, cipher_factory, key_length, block_size, md=hashlib.md5, salt: bytes=None):
    """python3 ./aes256-compat-openssl-enc-dec.py -in <filein> -e -salt -aes-256-cbc"""

    if salt is None:
        # TODO: Python 3.6's secrets module
        salt = os.urandom(PKCS5_SALT_LEN)

    key, iv = EVP_BytesToKey(key_length, block_size, md, salt, passphrase)

    cipher = cipher_factory(key, iv)

    encrypted = b""
    encrypted += OPENSSL_ENC_MAGIC
    encrypted += salt

    padding = b''
    while True:
        chunk = input.read(cipher.block_size)

        # PKCS#7 padding method
        if len(chunk) < cipher.block_size:
            remaining = cipher.block_size - len(chunk)
            padding = bytes([remaining] * remaining)

        encrypted += cipher.encrypt(chunk + padding)

        if padding:
            break

    # output.write(base64.b64encode(encrypted))
    output.write(encrypted)

def openssl_enc_decrypt(input: io.BytesIO, output: io.BytesIO, passphrase: bytes, cipher_factory, key_length, block_size, md=hashlib.md5):
    assert input.read(len(OPENSSL_ENC_MAGIC)) == OPENSSL_ENC_MAGIC, 'Bad Magic Number'
    salt = input.read(PKCS5_SALT_LEN)

    key, iv = EVP_BytesToKey(key_length, block_size, md, salt, passphrase)

    cipher = cipher_factory(key, iv)

    prefetch = chunk = None
    while True:
        chunk = prefetch
        prefetch = input.read(cipher.block_size)

        if chunk:
            chunk = cipher.decrypt(chunk)
            if not prefetch:
                chunk = chunk[:-chunk[-1]]
            output.write(chunk)

        if not prefetch:
            break

#if __name__ == '__main__':
#    parser = argparse.ArgumentParser()

#    parser.add_argument('-in', 
#                        dest='input', 
#                        nargs='?', 
#                        type=argparse.FileType('rb'), 
#                        default=sys.stdin.buffer, 
#                        help='Input file')

#    parser.add_argument('-out', 
#                        dest='output', 
#                        nargs='?', 
#                        type=argparse.FileType('wb'), 
#                        default=sys.stdout.buffer, 
#                        help='Output file')

#    group = parser.add_mutually_exclusive_group()

#    group.add_argument('-e', 
#                       dest='encrypt',
#                       action='store_true', 
#                       default=True, 
#                       help='Encrypt')

#    group.add_argument('-d', 
#                       dest='encrypt', 
#                       action='store_false', 
#                       help='Decrypt')

#    group = parser.add_mutually_exclusive_group()
#    group.add_argument('-v', dest='loglevel', action='count', help='Verbose output')
#    group.add_argument('-debug', dest='loglevel', action='store_const', const=4, help='Print debug info')


#    group = parser.add_mutually_exclusive_group()
#    group.add_argument('-k',
#                       dest='passphrase', 
#                       help='Passphrase')
#    group.add_argument('-K', 
#                       dest='passphrase', 
#                       type=binascii.a2b_hex, 
#                       help='Raw key, in hex')

#    group = parser.add_mutually_exclusive_group()
#    group.add_argument('-salt', dest='dummy', action='store_true', help='Use salt in the KDF (default)')
#    group.add_argument('-nosalt', dest='salt', action='store_const', const=b'', help='Do not use salt in the KDF')
#    parser.add_argument('-S', dest='salt', type=binascii.a2b_hex, help='Salt, in hex')

#    # parser.add_argument('-iv', dest='iv', type=binascii.a2b_hex, help='IV in hex')

#    parser.add_argument('-md', dest='md', type=lambda v: functools.partial(hashlib.new, v), default=hashlib.md5, help='Use specified digest to create a key from the passphrase')

#    group = parser.add_mutually_exclusive_group(required=True)

#    for key_size in AES.key_size:
#        prefix = '-aes-%d' % (key_size * 8, )
#        group.add_argument(prefix + '-cbc', 
#                           dest='cipher', 
#                           action='store_const', 
#                           const=(lambda key, iv: AES.new(key, AES.MODE_CBC, iv), 
#                                  key_size, AES.block_size)
#                           )
#        # -aes-128-ccm
#        #group.add_argument(prefix + '-cfb', dest='cipher', action='store_const', const=(lambda key, iv: AES.new(key, MODE_CFB, iv), key_size, AES.block_size))
#        ## -aes-128-cfb1
#        ## -aes-128-cfb8
#        #group.add_argument(prefix + '-ctr', dest='cipher', action='store_const', const=(lambda key, iv: AES.new(key, MODE_CTR, iv), key_size, AES.block_size))
#        #group.add_argument(prefix + '-ecb', dest='cipher', action='store_const', const=(lambda key, iv: AES.new(key, MODE_ECB, iv), key_size, AES.block_size))
#        ## -aes-128-gcm
#        ## -aes-128-ocb
#        #group.add_argument(prefix + '-ofb', dest='cipher', action='store_const', const=(lambda key, iv: AES.new(key, MODE_OFB, iv), key_size, AES.block_size))
#        ## -aes-128-xts
#        #group.add_argument('-aes%d' % (key_size * 8, ), dest='cipher', action='store_const', const=(lambda key, iv: AES.new(key, MODE_CBC, iv), key_size, AES.block_size))
#        # -aes128-wrap

#    arg = parser.parse_args()
#    arg.loglevel = max([logging.CRITICAL, logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG, logging.NOTSET][arg.loglevel:] + [logging.NOTSET])

#    if arg.passphrase is None:
#        arg.passphrase = getpass.getpass('password: ')
#    if isinstance(arg.passphrase, str):
#        arg.passphrase = arg.passphrase.encode('utf-8')

#    if arg.encrypt:
#        openssl_enc_encrypt(arg.input, arg.output, arg.passphrase, arg.cipher[0], arg.cipher[1], arg.cipher[2], arg.md, arg.salt)
#    else:
#        #print(arg.input, arg.output, arg.passphrase, arg.cipher[0], arg.cipher[1], arg.cipher[2], arg.md)
#        openssl_enc_decrypt(arg.input, arg.output, arg.passphrase, arg.cipher[0], arg.cipher[1], arg.cipher[2], arg.md)
