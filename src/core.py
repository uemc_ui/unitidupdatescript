# 10/14/19 Quinlin Riggs

import os
import hashlib
import xml.etree.ElementTree as ET

from pathlib import Path
from Crypto.Cipher import AES

from path_helpers import get_xml_path, get_sql_log_path
from crypto_helpers import openssl_enc_decrypt, openssl_enc_encrypt

def main(top_dir=None, unitid=None, smartpartnum=None):

    top_dir = Path(top_dir)

    xml_to_decrypt = get_xml_path(top_dir)
    sql_path = get_sql_log_path(top_dir)

    decrypted_xml_path = Path(str(xml_to_decrypt).replace('ExportClone', 
                                                          'TestOut'))

    # Decrypt Info
    f_in = open(xml_to_decrypt, 'rb')
    f_out = open(decrypted_xml_path, 'wb')
    key = b'ThermoFisherDeltaK-Network-Auth-Key'
    key_length = 32
    block_size = 16
    lib = hashlib.md5
    func = (lambda key, iv: AES.new(key, AES.MODE_CBC, iv))

    # Decrpyt
    openssl_enc_decrypt(f_in, f_out, key, func, key_length, block_size, lib)

    # Be sure to close
    f_in.close()
    f_out.close()

    # XML Parse
    tree = ET.parse(decrypted_xml_path)
    # Set new root
    x = tree.find('.//UnitInfo')
    # Clear old values due to degeneracy of the button tag
    x.clear()
    ET.SubElement(x, 'button', Name='UNITID', value='{0}'.format(unitid))
    ET.SubElement(x, 'button', Name='SMARTPARTNUM', 
                  value='{0}'.format(smartpartnum))

    new_xml_out = 'new_xml_out.xml'
    new_xml_out_enc = 'new_xml_out_enc.xml'

    new_xml_out_path = top_dir / new_xml_out
    new_xml_out_enc_path = top_dir / new_xml_out_enc

    tree.write(new_xml_out_path)

    f_in = open(new_xml_out_path, 'rb')
    f_out = open(new_xml_out_enc_path, 'wb')

    openssl_enc_encrypt(f_in, f_out, key, func, key_length, block_size, lib)

    f_in.close()
    f_out.close()

    # File Management
    if os.path.exists(new_xml_out_path):
        os.remove(new_xml_out_path)
    else:
        print('New XML does not exist')

    if os.path.exists(decrypted_xml_path):
        os.remove(decrypted_xml_path)
    else:
        print('Decrypted XML does not exist')

    if os.path.exists(new_xml_out_enc_path):
        os.replace(new_xml_out_enc_path, xml_to_decrypt)
    else:
        print('New Encrypted XML does not exist')

    if sql_path:
        if os.path.exists(sql_path):
            os.remove(sql_path)
        else:
            print('SQL Does not exist in clone')

    return True
