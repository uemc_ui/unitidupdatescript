Unit ID Updater

Usage:

	Windows:
		
		1. Nagivate to unitidupdatescript/dist/ and click gui.exe

	Linux/Unix:

		1. Nagivate to unitidupdatescript/dist/
		2. Run > pip install -r requirements.txt
		2. Run > python3 src/gui.py


This script will update the UnitID and SmartPartNum in the ExportClone.xml

Interface w/ DeltaK Steps:

	1.  Insert USB into DeltaK unit
	2.  Export DeviceClone to USB
	3.  Insert USB into Windows Computer
	4.  Open UpdateUnitInfo.exe (attached), a gui should open up
	5.  Inside of this gui, click the "Choose Unit Clone Directory" and 
	    choose the DeviceClone file that was just created via the 
	    Export DeviceClone
	6.  Enter a Unit ID between 0 and 1677215
	7.  Enter a SmartPart Number, this can be a combination of text and integers
	8.  Click the "Update Unit Information Button"
	9.  If the gui says "Unit Information Updated.", close the gui. 
	    Otherwise there has been an error in the gui.
	10. Eject the USB and re-insert into the DeltaK unit
	11. Import the DeviceClone via the DeltaK interface
	12. Verify that the UnitID and SmartPart Number on the DeltaK 
		interface is the same as what you entered on the gui